(import level8-mod classchemer racket-kwargs)

(print "test")

(define p2 (new Point2 1 2))

(print "created")

(print p2)

(print "printed")

(send p2 display)
(send p2 move #:dy 5)
(send p2 display)

(print "================================")

(define p3 (new Point3 1 2 3))

(print "--------------------------------")

(send p3 display)
(print "displayed")
(print (send p3 norm3))


(define p3b (new Point3 3 4 5))
(send p3b display)

(print (is-a? Point2 p3b))
(print (is-a? Point3 p3b))
(print (is-a? Point4 p3b))

(define p4 (new Point4 1 2 3 4))

(send p4 display)

(print (is-a? Point2 p4))
(print (is-a? Point3 p4))
(print (is-a? Point4 p4))

(print (responds-to? p4 norm3))

(send p4 asdf)

all: classchemer.import.scm racket-kwargs.import.scm level8-mod.import.scm level8

.PHONY: clean
clean:
	rm -f *.o *.import.scm *.link *.so

CSC=csc

%.o: %.scm
	$(CSC) -c -J -static $<
%.so: %.scm
	$(CSC) -shared $<

%.import.scm: %.scm
	$(CSC) -c -J $<

classchemer.o: classchemer.import.scm
classchemer.import.scm: classchemer.scm racket-kwargs.import.scm

racket-kwargs.o: racket-kwargs.import.scm
racket-kwargs.import.scm: racket-kwargs.scm

level8-mod.o: level8-mod.import.scm
level8-mod.import.scm: level8-mod.scm classchemer.import.scm racket-kwargs.import.scm

level8: level8.scm level8-mod.import.scm classchemer.import.scm racket-kwargs.import.scm
	$(CSC) $<

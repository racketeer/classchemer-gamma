;;
;; classchemer.scm
;;
;; Simple class-based object-oriented system with single-inheritance,
;; static private instance variables, public virtual methods bindings
;; and virtual method dispatching.
;;
;; ISC License
;;
;; Copyright 2024 Dominik Pantůček <dominik.pantucek@trustica.cz>
;;
;; Permission to use, copy, modify, and/or distribute this software
;; for any purpose with or without fee is hereby granted, provided
;; that the above copyright notice and this permission notice appear
;; in all copies.
;; 
;; THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
;; WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
;; AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
;; CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
;; OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
;; NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
;; CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
;;

(declare (unit classchemer))

(module
 classchemer
 (
  define-class
  new
  send

  super
  self

  is-a?
  responds-to?

  %:make-empty-instance
  %:instance-path
  %:instance-methods
  %:set-instance-methods!
  )

 (import scheme
	 (chicken base)
	 (chicken format)
	 racket-kwargs)

 ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Internal instance representation

 ;; Simple wrapper record allowing custom formating and generic-like
 ;; handling.
 (define-record-type instance
   (make-instance path methods)
   instance?
   (path %:instance-path)
   (methods %:instance-methods %:set-instance-methods!))

 ;; Creates instance record with only class inheritance path - as the
 ;; actual method closures have to be added after they are created
 ;; during instance allocation using `new`.
 (define (%:make-empty-instance path)
   (make-instance path '()))

 ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Forbidden bindings outside of the methods' bodies.

 ;; References the parent class' method of the same name as the method
 ;; this binding is encountered in.
 (define-syntax super
   (syntax-rules ()
     ((_ . x)
      (syntax-error "invalid usage of super"))))

 ;; References the current instance of the method's class.
 (define-syntax self
   (syntax-rules ()
     ((_ . x)
      (syntax-error "invalid usage of self"))))

 ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Run-time Type Information

 ;; Evaluates to #t if given instance is an instance of given class or
 ;; one of its descendants.
 (define-syntax is-a?
   (syntax-rules ()
     ((_ class inst)
      (let ((cp (class path)))
	(let loop ((ip (%:instance-path inst)))
	  (if (equal? ip cp)
	      #t
	      (if (null? ip)
		  #f
		  (loop (cdr ip)))))))))

 ;; Returns #t if given instance responds to given message
 (define-syntax responds-to?
   (syntax-rules ()
     ((_ inst method)
      (let ()
	(if (assq 'method (%:instance-methods inst)) #t #f)))))

 ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Class definitions

 ;; Creates new class represented as syntax transformer given by
 ;; class-name argument. Optionally can specify parent class.
 (define-syntax define-class
   (syntax-rules (define inherit self)

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 10 - The helper entry point - used both by `new` for creating
     ;; a new instance of the class and `define-class` for deriving
     ;; class that inherits from its parent. Also provides the classes
     ;; path from root class - used by `is-a?` predicate.
     ;;
     ;; First argument is the list of identifiers starting with the
     ;; class being created and continuing with its optional parents
     ;; up to the root of the class hierarchy.
     ;;
     ;; The second argument is a list of bodies - each body being a
     ;; list of class body expressions.

     ((_ 10 (class-name parent ...) (body ...))
      (define-syntax class-name
	(syntax-rules (instantiate derive path)
	  ;; Static list containing this class' name and chain of all
	  ;; parents up to the class hierarchy root (class without
	  ;; parent).
	  ((_ path)
	   '(class-name parent ...))

	  ;; Constructs the instance closure based on all the
	  ;; expressions gathered so far from all classes of the chain
	  ;; in the class hierarchy.
	  ((_ instantiate)
	   (define-class 20
	     (class-name parent ...)
	     () () () () (body ...)
	     (meiBai4ieshoos4yufaV7thi ieR7Aej4suHokah1kew3mak6 beequaePata5lei3oikee4ra Xeicef5Wa8yohng8ahvi2koh oleeceo1EeG5Ehoh4thie7ne oahoh5gaet9oorohp1roo9Ke ithae1tahfieph9cheengeiL Cee3Yooz1thajail4axooCha eikahhe4ail4EeyuMiduu8oi xeich4waivah5Gae7sa8uyul math7AhthovuriJiu8xeipha aeWafah8quei1Teigieraibe Aejie8quaelah5ush7ThaiM9 nuataekahm0tah4ahCohjohy rom6yaY0paaB9ne4ieshohqu Tahb1eixaNgech1Ua1ya4Roh baeB7een0ahbei2afiu9Zeuk Fei5Siew0fee9aNg4Ail0ahn ohLahph6uYahx2one4mohp1B sia4eiYahGhee5dooshah1go noh4quah1piJohchau4oiz6o Zaelaiphei3Ahaphu8heapho ahquiuc6aezi5eJeesaFaiNg tas2quoh1xeb5biem6Xe6uTe iekoh1Loz2joluuDe0Bai9ch uDieshoot4johfai0eijexoh Zahwoh6eimeeruediothahch peengohKeewoo1zahboh6hoh aeji4Ipoache3eengooph7th xosah7Aex5aewohYaephaeD8 eepaeSi3aeR5aebifahciele cho7zei3uag2Lae0eiph7rai EQuuu0Xoh8ahqu9ooqu8Ohng eighae4On7ohGhooph8Ia4ac Ahceog2leuTh8sive9pheb1C boo8goh0rei2puSh9fo8aire veu9quie0phe1aeFu3ahhaed ohk6Ohwee8EMeing2oobaegh ku7eix8Ooshahgh2thaiv4bi ael0Ohza3dieveiXahtheem6 einierahB3gouquoi9ceep8o Iihai1Ahngiey5ui7jib4coh Apoo3ain3wahzoo2nieth5ag iuqueequ7aiwieK3Eefae7la ohy4iip3uTh9ayeeviVeB8Wa geChaig7AhPh0shugahdeicu Hew1oroh5oonohv3jiethuan eecei3Shoh4phohfuTi7Zi7o Ue5ahthoogh9chae8iak9pe3 jaQu1quooboh5Qui8xeiphei eQuexeeLuZohpio9ohdeizoV phooBai4eakaetieyageZ4ee mahx0pi7liNeiruy2Ei5yiev roh7teit9eiQuaiFoo4haew9 aeb5aire9aiNifeiShafieSu zohGha9eis0vaithengiech1 shi1zai6eH5kiMecaQuaique maixa4eeT9diaFai4aph5she equ4eele5bosu7ajaiK9thaD ohng1TieYiiv8Ilahs6maij8 eel4ailaf9yeimetae2Suoch rooGiepikiep5neNoo0Faogh gee0eir0jaeChoofah3ban5i aikuhae1Dee1biep4zahdohG peikeeRaceequohr4aeju9cu iiW9WeoJah6Shoo8ieX3seip dooGaHie7oht8quaineoghee eik8ied9boowe4Ooleimohm4 phaqu2ohpeleiyoKoov1foal sair3aeV5ohj9iuxohshekei Ooyie9vi4eetoon8athah1oo BahJ8oefephiquaitheuz2ai iw3eabe0cheegu3oPhivei6f hetei6caiCue2goo6eifaeno hae2aiquui6Uehi5soi6choh Iim9ahthoo7guiGhohv6ohng kiep6ahlo3eighah6queeJae ohGievohgook9beelie5Sood foo5ahc2CahGhien3eig0Eeh nozaruphieC1uToecuvahpai ahraiSh8Uiy9phahn6naepha ahsh7Aucheecheirojongobu ojahdeedut6yooYaij4ahLae tah9aiXai9xiese6uu6oosho phishaeP1ohth3pheeHai0ju dau7nooVuph8neeth7me9eig yai1oozooh2eexei2piipu5M Eebahveethahmoh9Gavahh5e iehuengaihahqu5quasohSho Ooloo5fahtaxie5iovoPie3z aeT4Yiemoh5emahsahMeejoo eX3ethophoo6eingohleiJo4 ooTh3Ait5ceeshooT3cheime Shiecou2cee6eij8EeC2ay4r ohGohyahsh7kohkeiteiniew gae7oolaiTao3aetoob1ohMi eidai8asaeXaeraet5ur6yih iemeethohph1wie1wai7ciiY shaib0thai4Uquoo6yeichei iuMe9zex9xuwugh9Ith5fao7)))

	  ;; Syntax continuation that defines new syntax for the
	  ;; derived class while adding its expressions to the
	  ;; recorded class expressions (fields, methods and method
	  ;; inherits). The derived-body is placed at the end of the
	  ;; list of bodies.
	  ((_ derive derived-name derived-body)
	   (define-class 10
	     (derived-name class-name parent ...)
	     (body ... derived-body))))))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 20 Processing classes bodies
     ;;
     ;; Creates the instance record and initiates creating the nested
     ;; let and letrec expressions.
     ;;
     ;; Arguments:
     ;;
     ;; Class path - list of identifiers from this class up to the
     ;; root of the classes hierarchy.
     ;;
     ;; List of lists of fields.
     ;;
     ;; List of lists of methods.
     ;;
     ;; List of lists of method inherits.
     ;;
     ;; List of bodies (each body being a list of class body
     ;; expressions).

     ;; Final pass
     ((_ 20 class-path fields methods inherits vmt () tempsyms)
      (let ((self-record (%:make-empty-instance 'class-path)))
	(define-class 100 self-record class-path fields methods inherits vmt tempsyms)))

     ;; Start body processing, add new lists for this layer
     ((_ 20 class-path (lfield ...) (lmethod ...) (linherit ...) vmt
	 (body more-bodies ...)
	 tempsyms)
      (define-class 30 class-path
	(lfield ... ())
	(lmethod ... ())
	(linherit ... ())
	vmt
	(body more-bodies ...)
	tempsyms))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 30 - process single body of class expressions

     ;; Finished body processing
     ((_ 30 class-path fields methods inherits vmt (() more-bodies ...) tempsyms)
      (define-class 20 class-path fields methods inherits vmt (more-bodies ...) tempsyms))

     ;; Collecting pass: add method - starts with (()) for new-methods
     ;; as there is certainly a starting layer and it simplifies the
     ;; recursion later on
     ((_ 30 class-path fields methods inherits vmt
	 (((define (method-id . method-formals) . method-body) more-body ...) more-bodies ...)
	 tempsyms)
      (define-class 40 class-path fields methods inherits
	method-id method-formals method-body
	((more-body ...) more-bodies ...)
	(())
	vmt
	()
	tempsyms))
     
     ;; Collecting pass: add field
     ((_ 30 class-path fields methods inherits vmt
	 (((define field-id field-init-value) more-body ...) more-bodies ...)
	 tempsyms)
      (define-class 50 class-path fields methods inherits vmt
	(((define field-id field-init-value) more-body ...) more-bodies ...)
	tempsyms))

     ;; Collecting pass: inherit method(s)
     ((_ 30 class-path fields methods inherits vmt
	 (((inherit method-id more-method-ids ...) more-body ...) more-bodies ...)
	 tempsyms)
      (define-class 60 (class-path fields methods inherits vmt)
	(((inherit method-id more-method-ids ...) more-body ...) more-bodies ...)
	tempsyms))

     ;; Unknown expression in class body
     ((_ 30 . anything)
      (syntax-error "Unknown class expression" anything))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 40 - add method - firstly, we find the super-binding (if any)
     ;;
     ;; Arguments:
     ;;
     ;; class-path
     ;;
     ;; fields - list of lists
     ;;
     ;; old-methods - current list of lists, gets consumed to find
     ;; latest method of the same name
     ;;
     ;; inherits - list of lists
     ;;
     ;; method-id
     ;;
     ;; method-formals
     ;;
     ;; method-body
     ;;
     ;; class-bodies-rest: ((more-body ...) more-bodies ...)
     ;;
     ;; new-methods - reconstructed from old-methods
     ;;
     ;; old-vmt - current and unchanged VMT (in this stage)
     ;;
     ;; super-binding - if there is any method of the same name in
     ;; previous layers, this identifier should bind it

     ;; Finished, just add the method, no super binding - the (())
     ;; signals emptied last body
     ((_ 40 class-path fields (()) inherits
	 method-id method-formals method-body
	 more-bodies
	 new-methods
	 old-vmt
	 ()
	 tempsyms)
      (define-class 41 (class-path fields inherits old-vmt more-bodies)
	new-methods
	(method-id method-formals method-body ())
	method-body
	method-body
	tempsyms))

     ;; Finished, just add the method, postpone super binding - the
     ;; (()) signals emptied last body, add tempsym to super binding
     ((_ 40 class-path fields (()) inherits
	 method-id method-formals method-body
	 more-bodies
	 new-methods
	 old-vmt
	 super-binding
	 (temp-binding . tempsyms-rest))
      (define-class 41 (class-path fields inherits old-vmt more-bodies)
	new-methods
	(method-id method-formals method-body (temp-binding super-binding))
	method-body
	method-body
	tempsyms-rest))

     ;; Finished layer, more layers to go, add new method layer to
     ;; new-methods and start processing, adds as last (iterating old
     ;; methods from the first layer to the last one).
     ((_ 40 class-path fields (() more ...) inherits
	 method-id method-formals method-body
	 more-bodies
	 (new-methods ...)
	 old-vmt super-binding
	 tempsyms)
      (define-class 40 class-path fields (more ...) inherits
	method-id method-formals method-body
	more-bodies
	(new-methods ... ())
	old-vmt super-binding
	tempsyms))

     ;; One step over methods syntax list, iterating from first to
     ;; last, taking first from old, adding as last to new (like with
     ;; whole layers).
     ((_ 40 class-path fields
	 (((old-method-id . old-method-spec) . more-old-methods) more ...)
	 inherits
	 method-id method-formals method-body
	 more-bodies
	 (more-new-methods ... (new-method ...))
	 old-vmt super-binding
	 tempsyms)
      (let-syntax
	  ((if-method-id?
	    (syntax-rules (method-id)
	      ((_ method-id true false) true)
	      ((_ other-id true false) false))))
	(if-method-id?
	 old-method-id
	 ;; Update super binding with this old method
	 (define-class 40 class-path fields
	   (more-old-methods more ...)
	   inherits
	   method-id method-formals method-body
	   more-bodies
	   (more-new-methods ... (new-method ... (old-method-id . old-method-spec)))
	   old-vmt
	   old-method-id
	   tempsyms)
	 ;; Keep current super binding
	 (define-class 40 class-path fields
	   (more-old-methods more ...)
	   inherits
	   method-id method-formals method-body
	   more-bodies
	   (more-new-methods ... (new-method ... (old-method-id . old-method-spec)))
	   old-vmt
	   super-binding
	   tempsyms))))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 41 - add method self, the super-binding might be () if no
     ;; methods of the same name were defined earlier.
     ;;
     ;; Collects all self identifiers for their later binding.

     ;; Done, self exists
     ((_ 41 (class-path fields inherits vmt more-bodies)
	 (more-new-methods ... (new-method ...))
	 (method-id method-formals method-body method-super method-self ...)
	 (self . method-source-id-rest)
	 (binding . method-source-bind-rest)
	 tempsyms)
      (define-class 42
	(class-path
	 fields
	 (more-new-methods
	  ...
	  (new-method
	   ...
	   (method-id method-formals method-body method-super method-self ... binding)))
	 inherits more-bodies)
	method-id () vmt
	tempsyms))

     ;; Done, self does not exist
     ((_ 41 (class-path fields inherits vmt more-bodies)
	 (more-new-methods ... (new-method ...))
	 (method-id method-formals method-body method-super method-self ...)
	 ()
	 ()
	 tempsyms)
      (define-class 42
	(class-path
	 fields
	 (more-new-methods
	  ...
	  (new-method
	   ...
	   (method-id method-formals method-body method-super method-self ...)))
	 inherits more-bodies)
	method-id () vmt
	tempsyms))

     ;; One step splicing
     ((_ 41 common
	 new-methods
	 method-spec
	 ((identifier ...) . method-source-id-rest)
	 ((binding ...) . method-source-bind-rest)
	 tempsyms)
      (define-class 41
	common
	new-methods
	method-spec
	(identifier ... . method-source-id-rest)
	(binding ... . method-source-bind-rest)
	tempsyms))

     ;; One step direct
     ((_ 41 common
	 new-methods
	 method-spec
	 (identifier . method-source-id-rest)
	 (binding . method-source-bind-rest)
	 tempsyms)
      (define-class 41
	common
	new-methods
	method-spec
	method-source-id-rest
	method-source-bind-rest
	tempsyms))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 42 - Update VMT

     ;; Done, add the method id to the front of the VMT
     ((_ 42 (class-path fields methods inherits more-bodies)
	 method-id new-vmt () tempsyms)
      (define-class 30
	class-path fields methods inherits
	(method-id . new-vmt) more-bodies
	tempsyms))

     ;; Remove any instance of this method-id
     ((_ 42 common
	 method-id
	 new-vmt
	 (old-method-id . old-methods-rest)
	 tempsyms)
      (let-syntax
	  ((if-method-id?
	    (syntax-rules (method-id)
	      ((_ method-id true false) true)
	      ((_ other-id true false) false))))
	(if-method-id?
	 old-method-id
	 (define-class 42
	   common
	   method-id
	   new-vmt
	   old-methods-rest
	   tempsyms)
	 (define-class 42
	   common
	   method-id
	   (old-method-id . new-vmt)
	   old-methods-rest
	   tempsyms))))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 50 - add field to current layer and continue processing
     ;; current layer
     ((_ 50 class-path
	 (more-field-layers ... (field ...))
	 methods inherits vmt
	 (((define field-id field-init-value) more-body ...) more-bodies ...)
	 tempsyms)
      (define-class 30 class-path
	(more-field-layers ... (field ... (field-id field-init-value)))
	methods inherits vmt
	((more-body ...) more-bodies ...)
	tempsyms))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 60 - inherit method
     ;;
     ;; Arguments:
     ;;
     ;; common - all the arguments of 30 - single body processing
     ;;
     ;; more - the current list of bodies remaining to process

     ;; No identifiers remain
     ((_ 60 (class-path fields methods inherits vmt)
	 (((inherit) more-body ...) more-bodies ...)
	 tempsyms)
      (define-class 30
	class-path fields methods inherits vmt
	((more-body ...) more-bodies ...)
	tempsyms))

     ;; Some identifiers remain
     ((_ 60 (class-path fields methods inherits vmt)
	 (((inherit method-id more-method-ids ...) more-body ...) more-bodies ...)
	 tempsyms)
      (define-class 61
	(class-path fields methods inherits vmt
		    (((inherit more-method-ids ...) more-body ...) more-bodies ...))
	methods
	method-id
	()
	tempsyms))
     
     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 61 - inherit method
     ;;
     ;; Arguments:
     ;;
     ;; common - all the arguments of 30 - single body processing
     ;;
     ;; old-methods - list of lists of (id . spec) for all layers
     ;;
     ;; method-id
     ;;
     ;; super-id - most recent binding of the same name

     ;; If no method found, signal an error
     ((_ 61 common () method-id () tempsyms)
      (syntax-error "cannot inherit non-existent method" 'method-id))

     ;; Finished searching, found, go back to processing 
     ((_ 61 (class-path fields methods (more-inherited ... (inherited ...)) vmt more)
	 () method-id super-id
	 tempsyms)
      (define-class 60
	(class-path fields methods
		    (more-inherited ... (inherited ... (method-id super-id)))
		    vmt)
	more
	tempsyms))

     ;; Finished layer, potentially more layers to go
     ((_ 61 common (() more ...) method-id super-id tempsyms)
      (define-class 61 common (more ...) method-id super-id tempsyms))

     ;; One step through the methods of single layer
     ((_ 61 common
	 (((old-method-id . old-method-spec) . more-methods) more ...)
	 method-id super-id
	 tempsyms)
      (let-syntax
	  ((if-method-id?
	    (syntax-rules (method-id)
	      ((_ method-id true false) true)
	      ((_ other-id true false) false))))
	(if-method-id?
	 old-method-id
	 (define-class 61 common (more-methods more ...) method-id old-method-id tempsyms)
	 (define-class 61 common (more-methods more ...) method-id super-id tempsyms))))

     ((_ 60 more ...)
      (syntax-error "Got to 60 with " '(more ...)))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 100 - create the nested let/letrec for one layer (class) in the
     ;; inheritance chain if there is at least one layer to
     ;; process. Finish creating the instance and return the result if
     ;; there are none left.

     ;; Done
     ((_ 100 self-record (class-name parent-name ...)
	 () () () (table-entry ...) tempsyms)
      (let ()
	(%:set-instance-methods! self-record (list (cons 'table-entry table-entry) ...))
	self-record))

     ;; More to go
     ((_ 100 self-record (class-name parent-name ...)
	 ((field ...) more-fields ...)
	 methods
	 inherits
	 vmt
	 tempsyms)
      ;; Create the outer let with instance fields from this class
      (let (field ...)
	(define-class 101 self-record (class-name parent-name ...)
	  (more-fields ...)
	  methods
	  inherits
	  vmt
	  tempsyms
	  methods)))

     ((_ 100 rest ...)
      (syntax-error "Got to 100 with " '(rest ...)))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 101 - tempsym layer

     ;; No methods left
     ((_ 101 self-record (class-name parent-name ...)
	 fields
	 methods
	 inherits
	 vmt
	 tempsyms
	 (() more-methods ...))
      (define-class 102 self-record (class-name parent-name ...)
	fields
	methods
	inherits
	vmt
	tempsyms))

     ;; Method does not need alias
     ((_ 101 self-record (class-name parent-name ...)
	 fields
	 methods
	 inherits
	 vmt
	 tempsyms
	 (((method-name method-formals method-body () method-self ...) other-methods ...)
	  more-methods ...))
      (define-class 101 self-record (class-name parent-name ...)
	fields
	methods
	inherits
	vmt
	tempsyms
	((other-methods ...) more-methods...)))

     ;; Method needs alias
     ((_ 101 self-record (class-name parent-name ...)
	 fields
	 methods
	 inherits
	 vmt
	 tempsyms
	 (((method-name method-formals method-body (temp-binding super-binding) method-self ...) other-methods ...)
	  more-methods ...))
      (let ((temp-binding super-binding))
	(define-class 101 self-record (class-name parent-name ...)
	  fields
	  methods
	  inherits
	  vmt
	  tempsyms
	  ((other-methods ...) more-methods...))))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 102 - the methods letrec for current layer
     ((_ 102 self-record (class-name parent-name ...)
	 fields-rest
	 (((method-name method-formals method-body method-super method-self ...) ...)
	  more-methods ...)
	 ((inherited ...) more-inherited ...)
	 vmt
	 tempsyms)
      ;; Methods, inherited methods
      (let (inherited ...)
	(letrec ((method-name
		  (lambda** (method-name . method-formals)
			    (let ((method-self self-record) ...)
			      (define-class 11 super method-super method-body))))
		 ...)
	  ;; Loop to next round
	  (define-class 100 self-record (class-name parent-name ...)
	    fields-rest
	    (more-methods ...)
	    (more-inherited ...)
	    vmt
	    tempsyms))))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 11 - Capture literal with let
     ;;
     ;; Captures identifier - if it exists - and binds it to specified
     ;; value.
     ((_ 11 identifier () exprs)
      (let () . exprs))
     ((_ 11 identifier (replacement two) exprs)
      (letrec-syntax
	  ((capture0
	    (syntax-rules (identifier)
	      ((_ (identifier . srcs) (binding . srcsx) run)
	       (let ((binding replacement)) . run))
	      ((_ () () run)
	       (let () . run))
	      ((_ src srcx run)
	       (define-class 12 capture0 src run)))))
	(capture0 exprs exprs exprs)))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; 12 - Splice Head
     ;;
     ;; Helper syntax for capturing bindings
     ((_ 12 recur ((x ...) y ...) run)
      (recur (x ... y ...) (x ... y ...) run))
     ((_ 12 recur (y x ...) run)
      (recur (x ...) (x ...) run))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Derived class from parent (base) class
     ((_ (class-name parent) . body)
      (parent derive class-name body))

     ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
     ;; Root class without parent - a single class-name and class body
     ;; is present
     ((_ class-name . body)
      (define-class 10 (class-name) (body)))

     ))

 ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Handling of instances

 ;; Sends message to given class instance. As each class is a procedure
 ;; accepting one argument (the method name) and the resulting
 ;; procedure can accept arbitrary arguments (based on that method
 ;; definition), this is straightforward syntax.
 (define-syntax send
   (syntax-rules ()
     ((_ inst method-id a ...)
      ((lambda args
	 (let* ((methods (%:instance-methods inst))
		(method (assq 'method-id methods)))
	   (if method
	       (apply (cdr method) args)
	       (let ((fallback (assq 'unknown-method methods)))
		 (if fallback
		     (apply (cdr fallback) 'method-id args)
		     (error "No such method" 'method-id))))))
       a ...))))

 ;; Creates new instance of given class by instantiating the class and
 ;; sending an init message with arbitrary arguments to it.
 (define-syntax new
   (syntax-rules ()
     ((_ c a ...)
      (let ((i (c instantiate)))
	(when (responds-to? i init)
	  (send i init a ...))
	i))))

 ;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Allow custom printers
 (set-record-printer!
  instance
  (lambda (v out)
    (if (responds-to? v custom-print)
	(send v custom-print out)
	(fprintf out "#<~S>" (%:instance-path v)))))

 )

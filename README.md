
Classchemer
===========

R5RS-compatible Object Pascal-style OOP.

Proof of Concept
----------------

Run `make`

Run `./level8`

See `level8-mod.scm` for sample usage.



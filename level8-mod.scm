
(declare (unit level8-mod))

(module
 level8-mod
 (
  Point2 Point3 Point4
  )

 (import scheme
	 (chicken base)
	 classchemer
	 racket-kwargs
	 (chicken format))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 ;; Testing

 (define-class Point2
   (define x 0)
   (define y 0)
   (define (init x- y-)
     (print "init")
     (set! x x-)
     (set! y y-))
   (define (get-x) x)
   (define (get-y) y)
   (define (pascal)
     (+ x y))
   (define (norm2)
     (+ (* x x) (* y y)))
   (define (move #:dx (dx 0) #:dy (dy 0))
     (set! x (+ x dx))
     (set! y (+ y dy)))
   (define (display)
     (print x "," y " (" self ")")))

 (define-class (Point3 Point2)
   (define z 0)
   (inherit norm2 pascal)
   (define (display)
     (super)
     (print (pascal))
     (print " >> " z))
   (define (get-z) z)
   (define (norm3)
     (+ (norm2) (* z z)))
   (define (init x- y- z-)
     (super x- y-)
     (set! z z-)))

 (define-class (Point4 Point3)
   (define w 0)
   (define (unknown-method x . args)
     (print "unknown method " x " with " args))
   (inherit get-x get-y get-z)
   (define (custom-print out)
     (fprintf out "<[{~A,~A,~A,~A}]>"
	      (get-x) (get-y) (get-z) w))
   (define (init xx yy zz ww)
     (super xx yy zz)
     (set! w ww)))

 )
